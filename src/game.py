import sys
import time
from colorama import init, Fore, Back, Style

# Initializes Colorama
init(autoreset=True)

base_tomate_emoji = "\U0001F345 "
jambon_emoji = "\U0001F969 "
roquette_emoji = "\U0001F957 "
ananas_emoji = "\U0001F34D "
rucola_emoji = "\U0001f95E"
hawaienne_emoji = "\U0001f355"
feu_emoji = "\U0001F525"


class Jeu:
    def __init__(self) -> None:
        pass

    def start(self):
        while True:
            try:
                self.menu()
            except KeyboardInterrupt:
                print(" Exit game by Ctrl C !")
                sys.exit(0)

    def menu(self):
        print(
            "\n" + Style.BRIGHT + Fore.LIGHTGREEN_EX + "========" + Style.BRIGHT + Fore.LIGHTWHITE_EX + "MENU" + Style.BRIGHT + Fore.LIGHTRED_EX + "======== ")
        print("1 - Lancer le niveau 1.")  # niveau 1
        print("2 - Lancer le niveau 2.")  # niveau 2
        print("3 - Lancer le niveau 3.")  # niveau 3
        print("4 - Règles du jeu")  # règles du jeu
        print("5 - Quitter le jeu.")
        x = input("Choix : ")
        if x != '1' and x != '2' and x != '3' and x != '4' and x != '5':
            print("Choix impossible, entrez un choix possible")
            print()
        else:
            if int(x) == 1 or int(x) == 2 or int(x) == 3:

                partie = Partie()  # on démarre la partie
                jeu = Pizza(int(x))
                print(Style.BRIGHT + Fore.LIGHTGREEN_EX + "========" + Style.BRIGHT + Fore.LIGHTWHITE_EX + "Niveau "+str(x) + Style.BRIGHT + Fore.LIGHTRED_EX + "========"
                                                                                                                                                        "\n Dans ce niveau, Ernesto doit préparer "+str(jeu.res1-jeu.res2/7)+" Pizza Hawaienne et "+str(jeu.res2/7)+" Pizza Rucola en "+str(jeu.tempsimparti)+" secondes "
                                                                                                                                                        "\n Ernesto a exactement le bon nombre d'ingrédients")


                print("\n Chaud devant ! Cuisinons :")

                while jeu.tempsdejeu < jeu.tempsimparti and (
                        jeu.res1 + jeu.res2 + jeu.res3 + jeu.res4 + jeu.res5 + jeu.res6 + jeu.res7 + jeu.res13 != 0):  # tant que le joueur dispose de temps de jeu
                    try:
                        partie.affichage(jeu.res1, jeu.res2, jeu.res3, jeu.res4, jeu.res5, jeu.res6, jeu.res7, jeu.res8,
                                         jeu.res9, jeu.res10, jeu.res11, jeu.res12, jeu.res13)
                        # print(jeu.res1, jeu.res2, jeu.res3, jeu.res4, jeu.res5, jeu.res6,jeu.res7, jeu.res8, jeu.res9, jeu.res10,jeu.res11,  jeu.res12, jeu.res13)
                        # affichage(self, base, salade  , pineaplle    , ham, four1   , four2   , four3  , fouretat, pizzaH  , pizzaR   , temps   ,  score    , main ):
                        jeu.cuisine()
                        jeu.tempsdejeu = round((time.time() - jeu.debut_de_la_partie), 2)
                        print(
                            Style.BRIGHT + Fore.LIGHTMAGENTA_EX + "Temps de jeu: " + str(jeu.tempsdejeu) + " / " + str(
                                jeu.tempsimparti) + " secondes")

                    except ValueError:
                        print(Style.BRIGHT + Fore.LIGHTRED_EX + "Oops!  That was no valid number.  Try again...")
                jeu.score()

            elif int(x) == 4:
                print(
                    Style.BRIGHT + Fore.LIGHTGREEN_EX + "====================" + Style.BRIGHT + Fore.LIGHTWHITE_EX + "Règles du jeu" + Style.BRIGHT + Fore.LIGHTRED_EX + "=====================")
                print("")
                print(
                    Style.BRIGHT + Fore.LIGHTGREEN_EX + "Bienvenue dans Pizza Nostra, le célèbre jeu de fabrication de pizza")
                print("Vous y incarnerez Ernesto Nostra, le pizzaiolo")
                print(
                    Style.BRIGHT + Fore.LIGHTRED_EX + "Un certain nombre d'ingrédients seront disponibles , la partie se tiendra dans un temps imparti selon le niveau")
                print("")
                print("      ____                              ")
                print("     /    \	                            ")
                print("    |  u  u|       _______       Avec Ernesto, votre but sera  ")
                print("    |    \ |   .-''      ``-.        de confectionner des pizzas ")
                print("     \  = /   ((            ))            dans les mains d'Ernesto")
                print("     |  |     `-._      _.-'                  Et Ernesto ne peux porter qu'une pizza à la fois")
                print("     /\/\`--.    `-.""..-'        Au menu 2 pizza :      ")
                print(
                    "     |  |    \   /`./                _Pizza Hawaïenne " + hawaienne_emoji + " : 1 base tomate " + base_tomate_emoji + ", 3 morceaux de jambon " + jambon_emoji + ", 3 morceaux d'ananas " + ananas_emoji)
                print(
                    "     |\/|  \  `-'  /                 _Pizza Rucola " + rucola_emoji + ": 1 base tomate " + base_tomate_emoji + ", 7 lamelles de roquette " + roquette_emoji + ".")
                print(
                    "     || |   \     /                                         La roquette doit être ajoutée après cuisson   ")
                print(
                    " Les recettes ainsi réalisé seront ensuite à placer au four pour une cuisson de 30 seconde                   ")
                print(" Les Ingrédients disponibles sont affiché en haut de l'écran :                   ")
                print(" Base tomate " + base_tomate_emoji + str(0) + "| Roquette " + roquette_emoji + str(
                    0) + "| Ananas " + ananas_emoji + str(0) + "| Jambon " + jambon_emoji + str(0))
                print(
                    "                                                  ________________                                                    ________________")
                print(
                    " Voici le four éteint quand il est vide           |xx   ¤¤¤¤   xx|   Voici le four éteint quand il est rempli         |xx   ¤¤¤¤   xx|")
                print(
                    "                                                  |______________|                                                    |______________|")
                print(
                    "                                                  |______________|                                                    |______________|")
                print(
                    "                                                  |______________|            Une rucola et 2 hawaïenne               |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |")
                print(
                    "                                                  |______________|                                                    |______________|")
                print(
                    "                                                  |              |                                                    |              |")
                print(
                    "                                                  |______________|                                                    |______________|")
                print("")
                print("                                                  ________________")
                print(" Voici le four allumé quand il est rempli         |xx   ¤30¤   xx|")
                print("         On y vois un timer de 30 seconde         |______________|")
                print("                                                  |______________|")
                print(
                    "         Une rucola et 2 hawaïenne                |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |")
                print("                                                  |______________|")
                print(
                    "         Les bruleurs du four sont en route       |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |")
                print("                                                  |______________|")
                print("")
                print(
                    "Pour interagir avec les ingrédients il vous faudra utiliser le menu de cuisine : 1 - Faire une pizza")
                print("Pour mettre une pizza au four : 2 - Mettre la pizza faite au four")
                print("Pour interagir avec le four : 3 - Allumer le four")
                print(
                    "Pour éteindre le four et retirer les pizzas Hawaienne 4 - Éteindre le four et collecter les Hawaïennes si ils y en a")
                print(
                    "Pour éteindre le four et completer les base rucola 5 - Rajouter de la roquette sur une pizza base rucola cuite et collecter")
                print(" * les actions 4 et 5 éteignes le four et réalise leur action ")
                print("")
                print("En bas de votre écran vous trouverez vos quantité de pizza fini :")
                print(" Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                    1) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(1))
                print("")
                print("Dès la fin du temps imparti, le jeu prend fin et un score est calculé :")
                print("Base de pizza complète mais pas cuite - 4 points")
                print("Ingrédients restant -1 point ")
                print("Pizza terminée 10 points")

            elif int(x) == 5:
                self.quit()

    def quit(self):
        print("\nMerci et au revoir ...\n")

        sys.exit(0)


class Partie():
    def __init__(self) -> None:
        pass

    def affichage(self, base, salade, pineaplle, ham, four1, four2, four3, fouretat, pizzaH, pizzaR, temps, score,
                  main):

        if main == 0:
            # main vide
            if fouretat == False:
                # four éteint
                if four1 == 0 and four2 == 0 and four3 == 0:
                    # print("four vide")

                    print(
                        "______________________________________________________________________________________________________"
                        "\n| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                            salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                            ham) + " |                                         |"
                                   "\n|________________/______________/___________/______________/                                         |"
                                   "\n|                                                                                                    |"
                                   "\n|              _________                                                                             |"
                                   "\n|             /          \                                                                           |"
                                   "\n|             |           |                                                                          |"
                                   "\n|             \          /                                                                           |"
                                   "\n|      ____    \_  ___.-'                                                                            |"
                                   "\n|     /    \	 |/		                                                                             |"
                                   "\n|    |  u  u|       _______                                                       ________________   |"
                                   "\n|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |"
                                   "\n|     \  = /   ((             ))                                                  |______________|   |"
                                   "\n|     |  |     `-._       _.-'                                                    |______________|   |"
                                   "\n|     /\/\`--.    `-."".-'                                                          |______________|   |"
                                   "\n|     |  |    \   /`./                                                            |______________|   |"
                                   "\n|     |\/|  \  `-'  /                                                             |              |   |"
                                   "\n|     || |   \     /                                                              |______________|   |"
                                   "\n|___________________________________________                                                         |"
                                   "\n|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                            pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                            pizzaR) + "|                                                         |"
                                      "\n|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four éteint 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four éteint 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four éteint 2 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne et 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four éteint 3 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four éteint 3 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four éteint 1 hawaienne 2 rucola ")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four éteint 1 rucola 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

            if fouretat == True:
                # four allumé

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four allumé 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four allumé 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four allumé 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four allumé 3 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four allumé 3 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four allumé 1 hawaienne 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four allumé 1 rucola 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''       ``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((             ))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._       _.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

        if main == 1:
            # main base hawaienne
            if fouretat == False:
                # four éteint
                if four1 == 0 and four2 == 0 and four3 == 0:
                    # print("four vide")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |______________|   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four éteint 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four éteint 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four éteint 2 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne et 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four éteint 3 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four éteint 3 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four éteint 1 hawaienne 2 rucola ")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four éteint 1 rucola 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

            if fouretat == True:
                # four allumé

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four allumé 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four allumé 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four allumé 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four allumé 3 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four allumé 3 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four allumé 1 hawaienne 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four allumé 1 rucola 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       ___________                                                   ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.LIGHTRED_EX + jambon_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.WHITE + "``-.                                             |xx   ¤" + str(
                            temps) + "¤   xx|   |   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "#  #  #  #  #  #  #" + Style.BRIGHT + Fore.WHITE + "))                                            |______________|   |")
                    print(
                        "|     |  |     `-._ " + Style.BRIGHT + Fore.RED + jambon_emoji + "# " + Style.BRIGHT + Fore.YELLOW + ananas_emoji + Style.BRIGHT + Fore.RED + "# " + Style.BRIGHT + Fore.RED + jambon_emoji + Style.BRIGHT + Fore.WHITE + "_.-'                                            |______________|   |")
                    print(
                        "|     /\/\`--.    `--...""...--'                                                    |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

        if main == 2:
            # main base rucola
            if fouretat == False:
                # four éteint
                if four1 == 0 and four2 == 0 and four3 == 0:
                    # print("four vide")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |______________|   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four éteint 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four éteint 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four éteint 2 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four éteint 1 hawaienne et 1 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four éteint 3 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four éteint 3 rucola")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four éteint 1 hawaienne 2 rucola ")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four éteint 1 rucola 2 hawaienne")

                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤¤¤¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |              |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

            if fouretat == True:
                # four allumé

                if four1 == 1 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + hawaienne_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 0 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 0 and four3 == 2:
                    print("four allumé 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |      " + rucola_emoji + "      |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 0 and four3 == 1:
                    print("four allumé 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + hawaienne_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 0 and four3 == 2:
                    print("four allumé 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 0 or four1 == 0 and four2 == 1 and four3 == 2 or four1 == 1 and four2 == 0 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 0 or four1 == 0 and four2 == 2 and four3 == 1 or four1 == 2 and four2 == 0 and four3 == 1:
                    print("four allumé 1 hawaienne 1 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + "   " + rucola_emoji + "    |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 1:
                    print("four allumé 3 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 2:
                    print("four allumé 3 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 1 and four2 == 2 and four3 == 2 or four1 == 2 and four2 == 1 and four3 == 2 or four1 == 2 and four2 == 2 and four3 == 1:
                    print("four allumé 1 hawaienne 2 rucola")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + hawaienne_emoji + " " + rucola_emoji + " " + rucola_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

                if four1 == 2 and four2 == 1 and four3 == 1 or four1 == 1 and four2 == 2 and four3 == 1 or four1 == 1 and four2 == 1 and four3 == 2:
                    print("four allumé 1 rucola 2 hawaienne")
                    print(
                        "______________________________________________________________________________________________________")
                    print("| Base tomate " + base_tomate_emoji + str(base) + "| Roquette " + roquette_emoji + str(
                        salade) + "| Ananas " + ananas_emoji + str(pineaplle) + "| Jambon " + jambon_emoji + str(
                        ham) + " |                                         |")
                    print(
                        "|________________/______________/___________/______________/                                         |")
                    print(
                        "|                                                                                                    |")
                    print(
                        "|              _________                                                                             |")
                    print(
                        "|             /          \                                                                           |")
                    print(
                        "|             |           |                                                                          |")
                    print(
                        "|             \          /                                                                           |")
                    print(
                        "|      ____    \_  ___.-'                                                                            |")
                    print(
                        "|     /    \	 |/		                                                                             |")
                    print(
                        "|    |  u  u|       _______                                                       ________________   |")
                    print(
                        "|    |    \ |   .-''" + Style.BRIGHT + Fore.RED + "#%&#&%#" + Style.BRIGHT + Fore.WHITE + "``-.                                                   |xx   ¤" + str(
                            temps) + "¤   xx|   |")
                    print(
                        "|     \  = /   ((" + Style.BRIGHT + Fore.RED + "%&#&#&%&VK&%&" + Style.BRIGHT + Fore.WHITE + "))                                                  |______________|   |")
                    print(
                        "|     |  |     `-._" + Style.BRIGHT + Fore.RED + "#%&##&%" + Style.BRIGHT + Fore.WHITE + "_.-'                                                    |______________|   |")
                    print(
                        "|     /\/\`--.    `-."".-'                                                          |    " + rucola_emoji + " " + hawaienne_emoji + " " + hawaienne_emoji + "   |   |")
                    print(
                        "|     |  |    \   /`./                                                            |______________|   |")
                    print(
                        "|     |\/|  \  `-'  /                                                             |   " + feu_emoji + feu_emoji + feu_emoji + feu_emoji + feu_emoji + "   |   |")
                    print(
                        "|     || |   \     /                                                              |______________|   |")
                    print(
                        "|___________________________________________                                                         |")
                    print("|Pizza Hawaïenne : " + hawaienne_emoji + " " + str(
                        pizzaH) + "|" + " Pizza Rucola : " + rucola_emoji + " " + str(
                        pizzaR) + "|                                                         |")
                    print(
                        "|_____________________/____________________/_________________________________________________________|")

class Pizza():
    def __init__(self,level) -> None:
        # valeur commune au propre au scénario 1
        self.niveau
        Pizza.niveau(self, level)

    def niveau(self,choix) -> None:
        self.choix = choix

        if self.choix == 1:
            #niveau 1
            self.tempsimparti = 180
            self.res1 = 5  # base 5
            self.res2 = 14  # salade 14
            self.res3 = 9  # ham 9
            self.res4 = 9  # pineaplle 9
            self.res5 = 0  # emplacement four1
            self.res6 = 0  # emplacement four2
            self.res7 = 0  # emplacement four3
            # valeur commune au différents scénario
            self.debut_de_la_partie = time.time()
            self.tempsdejeu = 0
            self.resulthawaïennes = [1, 3, 3]
            self.resultbaserucolas = [1, 0, 0]
            self.res8 = False  # fouretat
            self.res9 = 0  # pizzaH conçu
            self.res10 = 0  # pizzaR conçu
            self.res11 = 30  # temps du four
            self.res12 = 0  # score du joueur
            self.res13 = 0  # main de ernesto joueur
            self.res14 = 0  # pizza à cuire
            self.res15 = 0  # pizza cuite

        elif self.choix == 2:
            # niveau 2
            self.tempsimparti = 260
            self.res1 = 12  # base 5
            self.res2 = 28  # salade 14
            self.res3 = 24  # ham 9
            self.res4 = 24  # pineaplle 9
            self.res5 = 0  # emplacement four1
            self.res6 = 0  # emplacement four2
            self.res7 = 0  # emplacement four3
            # valeur commune au différents scénario
            self.debut_de_la_partie = time.time()
            self.tempsdejeu = 0
            self.resulthawaïennes = [1, 3, 3]
            self.resultbaserucolas = [1, 0, 0]
            self.res8 = False  # fouretat
            self.res9 = 0  # pizzaH conçu
            self.res10 = 0  # pizzaR conçu
            self.res11 = 30  # temps du four
            self.res12 = 0  # score du joueur
            self.res13 = 0  # main de ernesto joueur
            self.res14 = 0  # pizza à cuire
            self.res15 = 0  # pizza cuite
        elif self.choix == 3:
            # niveau 3
            # niveau 1
            self.tempsimparti = 250
            self.res1 = 12  # base 5
            self.res2 = 49  # salade 14
            self.res3 = 15  # ham 9
            self.res4 = 15  # pineaplle 9
            self.res5 = 0  # emplacement four1
            self.res6 = 0  # emplacement four2
            self.res7 = 0  # emplacement four3
            # valeur commune au différents scénario
            self.debut_de_la_partie = time.time()
            self.tempsdejeu = 0
            self.resulthawaïennes = [1, 3, 3]
            self.resultbaserucolas = [1, 0, 0]
            self.res8 = False  # fouretat
            self.res9 = 0  # pizzaH conçu
            self.res10 = 0  # pizzaR conçu
            self.res11 = 30  # temps du four
            self.res12 = 0  # score du joueur
            self.res13 = 0  # main de ernesto joueur
            self.res14 = 0  # pizza à cuire
            self.res15 = 0  # pizza cuite

        """# valeur commune au propre au scénario 1
        self.tempsimparti = 180
        self.res1 = 5 #base 5
        self.res2 = 14 #salade 14
        self.res3 = 9  # ham 9
        self.res4 = 9 #pineaplle 9
        self.res5 = 0 # emplacement four1
        self.res6 = 0 # emplacement four2
        self.res7 = 0 # emplacement four3
        #valeur commune au différents scénario
        self.debut_de_la_partie = time.time()
        self.tempsdejeu = 0
        self.resulthawaïennes = [1, 3, 3]
        self.resultbaserucolas = [1, 0, 0]
        self.res8 = False #fouretat
        self.res9 = 0 #pizzaH conçu
        self.res10 = 0 #pizzaR conçu
        self.res11 = 30 #temps du four
        self.res12 = 0 #score du joueur
        self.res13 = 0 # main de ernesto joueur
        self.res14 = 0  # pizza à cuire
        self.res15 = 0  # pizza cuite"""

    def cuisine(self):
        ingredient = [0, 0, 0]
        print("\n ========Cuisinons======== "
              "\n1 - Faire une pizza"
              "\n2 - Mettre la pizza faite au four"
              "\n3 - Allumer le four"
              "\n4 - Éteindre le four et collecter les Hawaïennes si ils y en a"
              # "\n4 - Sortir une pizza"
              "\n5 - Rajouter de la roquette sur une pizza base rucola cuite et collecter")
        y = input("Choix : ")
        if y != '1' and y != '2' and y != '3' and y != '4' and y != '5':
            print(Style.BRIGHT + Fore.LIGHTRED_EX + "Choix impossible, entrez un choix possible")
        else:
            if int(y) == 1:  # faire une pizza
                if self.res13 != 0:
                    print(Style.BRIGHT + Fore.LIGHTRED_EX + "Une pizza est déjà dans les mains de Ernesto")
                else:
                    print("Faire une pizza :")
                    tomates = int(input("Nombre de base tomate: "))
                    ingredient[0] = tomates
                    jambon = int(input("Nombre de morceaux de jambon: "))
                    ingredient[1] = jambon
                    ananas = int(input("Nombre de morceaux d'ananas: "))
                    ingredient[2] = ananas

                    if ingredient != self.resulthawaïennes and ingredient != self.resultbaserucolas:
                        print(Style.BRIGHT + Fore.LIGHTRED_EX + "Recette incorrect")
                    else:
                        if self.res1 - tomates < 0 or self.res3 - jambon < 0 or self.res4 - ananas < 0:
                            print(Style.BRIGHT + Fore.LIGHTRED_EX + "Plus assez d'ingrédients pour cette recette")
                        else:
                            if ingredient == self.resulthawaïennes:
                                self.res13 = 1  # on met une pizza base hawaienne dans la main de Ernesto
                                # on consomme les ingredients
                                self.res1 = self.res1 - tomates
                                self.res3 = self.res3 - jambon
                                self.res4 = self.res4 - ananas
                                print(
                                    Style.BRIGHT + Fore.LIGHTCYAN_EX + "Base Pizza Hawaïenne dans les mains de Ernesto")
                            elif ingredient == self.resultbaserucolas:
                                self.res13 = 2  # on met une pizza base de rucola dans la main de Ernesto
                                # on consomme les ingredients
                                self.res1 = self.res1 - tomates
                                print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "Base Pizza Rucola dans les mains de Ernesto")

            elif int(y) == 2:  # Mettre la pizza faite au four
                print("Mettre la pizza faite au four :")
                if self.res13 == 0:
                    print(Style.BRIGHT + Fore.LIGHTRED_EX + "Ernesto n'a pas de pizza à mettre au four")
                else:
                    if self.res15 != 0:
                        print(
                            Style.BRIGHT + Fore.LIGHTRED_EX + "Pizza cuite encore présente dans le four, tout vider avant de remplir à nouveau")
                    else:
                        if self.res8 == True:
                            print(Style.BRIGHT + Fore.LIGHTRED_EX + "Impossible le four est allumé")
                        else:
                            if self.res5 != 0 and self.res6 != 0 and self.res7 != 0:
                                print(Style.BRIGHT + Fore.LIGHTRED_EX + "Four plein")
                            else:
                                if self.res5 == 0:
                                    self.res5 = self.res13  # on met la pizza de la main de Ernesto dans le four
                                    self.res13 = 0
                                    self.res14 = self.res14 + 1
                                elif self.res6 == 0:
                                    self.res6 = self.res13  # on met la pizza de la main de Ernesto dans le four
                                    self.res13 = 0
                                    self.res14 = self.res14 + 1
                                elif self.res7 == 0:
                                    self.res7 = self.res13  # on met la pizza de la main de Ernesto dans le four
                                    self.res13 = 0
                                    self.res14 = self.res14 + 1




            elif int(y) == 3:  # Allumer le four
                allumFour = (input("allumer le four o pour oui n pour non: "))
                if allumFour == "o":
                    self.res8 = True
                    print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "le four cuit les pizza en 30 sec")
                    time.sleep(30)
                    self.res15 = self.res14
                    self.res14 = 0
                    print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "les pizza sont cuites")

            elif int(y) == 4:
                print(" Eteindre le four et collecter les Hawaïennes si ils y en a :")
                self.res8 = False
                if self.res5 == 1:
                    self.res5 = 0
                    self.res9 = self.res9 + 1
                    self.res15 = self.res15 - 1
                if self.res6 == 1:
                    self.res6 = 0
                    self.res9 = self.res9 + 1
                    self.res15 = self.res15 - 1
                if self.res7 == 1:
                    self.res7 = 0
                    self.res9 = self.res9 + 1
                    self.res15 = self.res15 - 1

                if self.res5 == 2 and self.res6 != 2 and self.res7 != 2 or self.res5 != 2 and self.res6 == 2 and self.res7 != 2 or self.res5 != 2 and self.res6 != 2 and self.res7 == 2:
                    print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "au moins 1 rucola a besoin de roquette pour être fini")
                if self.res5 == 2 and self.res6 == 2 and self.res7 != 2 or self.res5 == 2 and self.res6 != 2 and self.res7 == 2 or self.res5 != 2 and self.res6 == 2 and self.res7 == 2:
                    print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "au moins 2 rucola ont besoin de roquette pour être fini")
                if self.res5 == 2 and self.res6 == 2 and self.res7 == 2:
                    print(Style.BRIGHT + Fore.LIGHTCYAN_EX + "3 rucola ont besoin de roquette pour être fini")


            elif int(y) == 5:
                print(" Rajouter de la roquette sur une pizza base rucola cuite et collecter :")
                if self.res5 == 2 or self.res6 == 2 or self.res7 == 2:
                    self.res8 = False
                    roquet = int(input("Nombre de roquette: "))
                    if self.res5 == 2 and roquet == 7:
                        self.res5 = 0
                        self.res10 = self.res10 + 1
                        self.res2 = self.res2 - roquet
                        self.res15 = self.res15 - 1
                        roquet = 0
                    if self.res6 == 2 and roquet == 7:
                        self.res6 = 0
                        self.res10 = self.res10 + 1
                        self.res2 = self.res2 - roquet
                        self.res15 = self.res15 - 1
                        roquet = 0
                    if self.res7 == 2 and roquet == 7:
                        self.res7 = 0
                        self.res10 = self.res10 + 1
                        self.res2 = self.res2 - roquet
                        self.res15 = self.res15 - 1
                        roquet = 0
                else:
                    print(Style.BRIGHT + Fore.LIGHTRED_EX + "Pas de base rucola ou pas de roquette")

    def score(self):

        self.res12 = self.res12 + self.res1 * -1 + self.res2 * -1 + self.res3 * -1 + self.res3 * -1 + self.res9 * 10 + self.res10 * 10

        if self.res5 == 1:
            self.res12 = self.res12 + self.res5 * -4
        elif self.res5 == 2:
            self.res12 = self.res12 + self.res5 * -2

        if self.res6 == 1:
            self.res12 = self.res12 + self.res6 * -4
        elif self.res6 == 2:
            self.res12 = self.res12 + self.res6 * -2

        if self.res7 == 1:
            self.res12 = self.res12 + self.res7 * -4
        elif self.res7 == 2:
            self.res12 = self.res12 + self.res7 * -2

        if self.res13 == 1:
            self.res12 = self.res12 + self.res13 * -4
        elif self.res13 == 2:
            self.res12 = self.res12 + self.res13 * -2

        if self.res1 + self.res2 + self.res3 + self.res4 + self.res5 + self.res6 + self.res7 + self.res13 == 0:
            print(Style.BRIGHT + Fore.GREEN + "Fin de la partie")
            print(Style.BRIGHT + Fore.GREEN + "Bravo, Vous avez parfaitement su aidé Ernesto en : " + str(
                self.tempsdejeu) + " il ne reste plus d'ingrédients")
            print(Style.BRIGHT + Fore.GREEN + "Voici votre score : " + str(self.res12))
        else:
            print(Style.BRIGHT + Fore.GREEN + "Fin de la partie")
            print(Style.BRIGHT + Fore.GREEN + "Perdu")
            print(Style.BRIGHT + Fore.GREEN + "Vous avez aidé tant que possible Ernesto en : " + str(
                self.tempsdejeu) + " mais vous n'avez plus le temps")
            print(Style.BRIGHT + Fore.RED + "Voici votre score : " + str(self.res12))


if __name__ == "__main__":
    print(" Bienvenue sur Pizza Nostra, Le célèbre jeu de simulation de pizzeria")
    j = Jeu()
    j.start()
